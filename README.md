# Instalare Telegram pe Fedora

Acest script Bash actualizează sistemul Fedora, instalează RPM Fusion și apoi instalează aplicația Telegram Desktop. 

## Cerințe

- Sistem de operare Fedora.
- Drepturi de administrator (root).

## Instrucțiuni de utilizare

1. Descărcați sau clonați acest depozit pe mașina dvs. locală.
2. Asigurați-vă că scriptul are permisiuni de execuție. Dacă nu are, rulați următoarea comandă:
    ```bash
    chmod +x install_telegram.sh
    ```
3. Rulați scriptul folosind comanda:
    ```bash
    sudo ./install_telegram.sh
    ```

## Descriere Script

Scriptul efectuează următoarele acțiuni:

1. Verifică dacă utilizatorul este root. Dacă scriptul nu este rulat ca root, afișează un mesaj de eroare și se oprește.
2. Actualizează sistemul folosind `dnf update`.
3. Instalează RPM Fusion, care este un set de repository-uri pentru Fedora ce oferă software suplimentar.
4. Instalează aplicația Telegram Desktop.

## Observații

- Acest script este destinat utilizării pe Fedora și poate să nu funcționeze corect pe alte distribuții Linux.
- Asigurați-vă că aveți o conexiune la internet stabilă pentru a descărca și instala pachetele necesare.
