#!/bin/bash

# Verifică dacă utilizatorul este root
if [[ $EUID -ne 0 ]]; then
   echo "Acest script trebuie să fie rulat ca root" 
   exit 1
fi

# Actualizează sistemul
sudo dnf update -y

# Instalează RPM Fusion
sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm -y

# Instalează Telegram
sudo dnf install telegram-desktop -y

echo "Telegram a fost instalat cu succes!"

